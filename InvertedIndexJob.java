import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class InvertedIndexJob {
	public static void main(String []args) throws IOException, ClassNotFoundException, InterruptedException{
		Job job = new Job();
		job.setJarByClass(InvertedIndexJob.class);
		job.setJobName("Inverted Index Job");
	    job.setMapperClass(WordToDocIDMapper.class);
	    job.setReducerClass(WordToDocIDReducer.class);
	    job.setOutputKeyClass(Text.class);	
	    job.setOutputValueClass(LongWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
	public static class WordToDocIDMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
		
		
		private Text word = new Text();

		public void map(LongWritable mapperKey, Text value, Context context) throws IOException, InterruptedException {
			//String currentLine = value.toString();
			StringTokenizer itr = new StringTokenizer(value.toString());
			private LongWritable documentId = new LongWritable();
			Long docId = Long.parseLong(itr.nextToken());
			documentId.set(docId);

			while(itr.hasMoreTokens()){
				word.set(itr.nextToken());
				context.write(word, documentId);
			}
		}
	}

	public static class WordToDocIDReducer extends Reducer<Text, LongWritable, Text, Text> {
		public void reduce(Text word, Iterable<LongWritable> docIdCount, Context context) throws IOException, InterruptedException{
			Map<String, Integer> map = new HashMap<>();
			for(LongWritable d: docIdCount) {
				if(!map.containsKey(d+"")){
					map.put(d+"", 1);
				}else{
					map.put(d+"", map.get(d+"") + 1);
				}
			}
			StringBuilder sb = new StringBuilder();
			for(String temp : map.keySet()) {
		        sb.append(temp + ":" + map.get(temp) + "\t");
		    }

            context.write(word, new Text(sb.toString()));
		}
	}
}